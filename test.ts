// This is a sample typescript file to verify Vim/CoC/LSP all work as intended
// Try: gd to goto definitions
//      gy to goto type definitions
//      gi to goto implemention
//      gr to goto references
//      Shift-K to see autocomplete/type documentation
//      <leader>f to format selected code (leader is currently ",")
//      <leader>ac to autocorrect code
//      <leader>af to autofix code
const numbers: number[] = [1, 3, 5, 7];

const numbersMap: number[] = numbers.map((num) => {
  return num + 1;
});

const booleans: boolean[] = [true, 0, "false"];

const someFunFunction = () => {
  return Math.random();
};

class Fruits {

  constructor(color: string) {
    this.color = color
  }
}
console.log(numbersMap);
